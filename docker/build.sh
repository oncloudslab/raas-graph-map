#!/bin/bash

usage_exit() {
	echo "Usage: $0 [-c]" 1>&2
	echo "-c: use cache, if you change only ras software, then use this option for saving time"
	exit 1
}

while getopts gstcmnh OPT
do
	case $OPT in
		c) CACHE=true
		   echo "use chache option"
		   ;;
		h) usage_exit
		   ;;
		\?) usage_exit
		   ;;
	esac
done

if [ $CACHE ]; then
	CACHE_OPT="--no-cache=false"
else
	CACHE_OPT="--no-cache=true"
fi

DOCKER=docker
FILE=Dockerfile
echo "$DOCKER build $CACHE_OPT -f $FILE --tag graph-os ./../"
sudo $DOCKER build $CACHE_OPT -f Dockerfile.nvidia --tag graph-os ./../

echo "$DOCKER build $CACHE_OPT -f $FILE --tag graph-map ./../"
sudo $DOCKER build $CACHE_OPT -f $FILE --tag graph-map ./../