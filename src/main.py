#!/usr/bin/python3
# -*- coding: utf-8 -*-
import boto3
import json
import yaml
from PIL import Image
import io
import cv2
import numpy as np
import time

#gRPC
import grpc
import grpc_proto
from concurrent import futures

#raas lib
import path
import orm

# copy from pathpoint class to grpc
def copy_pathpoint(p):
    pos = grpc_proto.raas_pb2.Point3d(x=str(p.pos[2]), y=str(p.pos[1]), z=str(p.pos[0]))
    floor_pos = grpc_proto.raas_pb2.FloorPoint(pos=pos, floor=str(p.floor))
    pp = grpc_proto.raas_pb2.WayPoint(floor_p=floor_pos, attribute=str(p.attribute))
    return pp

def copy_to_pathpoint(grpc_pp):
    pos = (float(grpc_pp.floor_p.pos.z), float(grpc_pp.floor_p.pos.y), float(grpc_pp.floor_p.pos.x) )
    print(pos)
    print(grpc_pp.floor_p)
    print(grpc_pp.floor_p.floor)
    print(grpc_pp.attribute)
    return( path.pathpoint.path_point( pos, int(grpc_pp.floor_p.floor), grpc_pp.attribute ) )

def copy_edges(edges):
    grpc_edges = []
    for e in edges:
        p2p =[]
        for p in e.p2p:
            pp = copy_pathpoint(p)
            p2p.append(pp)

        edge = grpc_proto.raas_pb2.Edge(p2p=p2p, cost=e.cost)
        grpc_edges.append(edge)
    return grpc_edges

def copy_path(path):
    grpc_path = []
    for p in path:
        pp = copy_pathpoint(p)
        grpc_path.append(pp)
    
    return(grpc_path)

class graph_grpc(grpc_proto.graph_pb2_grpc.GraphMapService):
    graph = None

    def __init__(self):
        super(graph_grpc, self).__init__()
        self.graph = graph_proc()
    
    def createGraph(self, request, context):
        print("request")
        print(request)

        self.graph.create_new_graph(request.floor)
        response = grpc_proto.raas_pb2.Result(result="True")
        return(response)

    def getGraph(self, request, context):
        print(request)
        edges = self.graph.get_graph(request.facility_id)
        for e in edges[0:3]:
            e.print()

        grpc_edges = copy_edges(edges)
        response = grpc_proto.raas_pb2.GraphMap(vertexes=[], edges=grpc_edges)
        return(response)

    def findPath(self, request, context):
        print("request")
        print(request)

        wp = []
        for grpc_wp in request.waypoints:
            wp.append( copy_to_pathpoint(grpc_wp) )

        path, cost = self.graph.find_path(request.facility, wp)
        grpc_path = copy_path(path)
        response = grpc_proto.raas_pb2.Path(path=grpc_path)
        return(response)

class graph_proc:
    db = None
    s3 = None
    BUCKETNAME = 'onclouds-raas-dev'

    #neo4j
    NEO4J_CONFIG = 'key/Neo4j_enterprise.yaml'

    def __init__(self):
        #aws
        boto3config = json.load(open('key/passwd-s3fs.json'))
        session = boto3.Session(aws_access_key_id=boto3config['accessKeyId'],
                  aws_secret_access_key=boto3config['secretAccessKey'],
                  region_name='ap-northeast-1')

        #s3
        self.s3 = session.resource('s3')
        for bucket in self.s3.buckets.all():
            print(bucket.name)

        #DB ( ORM )
        with open('db-config.json', 'r', encoding='utf-8') as conf:
            config = json.load(conf)
            DBconf = config[config['env']]['mapDB']
            self.db = orm.DBModel.MapDB(DBconf['host'], DBconf['port'], DBconf['username'],
                       DBconf['password'], DBconf['database'])

    def get_graph(self,map_id):
        graph = path.voronoi2gdb.graph_map(self.NEO4J_CONFIG)
        edges = graph.read_graph(map_id)
        return( edges )

    def get_unique_list(self,seq):
        seen = []
        return [x for x in seq if x not in seen and not seen.append(x)]

    def create_new_graph(self, map_id):
        map = self.db.get_slam_map(map_id)
        print(map[3])

        #read yaml file
        yaml_file_name = map[3]
        response = self.s3.Object(self.BUCKETNAME, yaml_file_name).get()
        body = response['Body'].read()
        yaml_obj = yaml.safe_load(body.decode('utf-8'))
        yamlData = {
                    "origin": {"x": yaml_obj['origin'][0], "y": yaml_obj['origin'][1]},
                    "resolution": {"x": yaml_obj['resolution'], "y": yaml_obj['resolution']},
                    "direction": {"x": 1, "y": -1}
        }

        #read pgm file
        pgm_file_name = map[2]
        response = self.s3.Object(self.BUCKETNAME, pgm_file_name).get()
        pgm_data = response['Body'].read()
        inst = io.BytesIO(pgm_data)
        # => <class 'bytes'>
        #image = Image.open(inst)
        #print(image.format)
        #print(image.mode)
        
        image = cv2.imdecode(
            np.array(bytearray(pgm_data), dtype=np.uint8),
            cv2.IMREAD_COLOR
            #cv2.IMREAD_UNCHANGED
        )

        #
        image_resize = cv2.resize(image, dsize=None, fx=0.5, fy=0.5)
        print("map size ", image_resize.shape)

        floor = 1
        graph = path.voronoi2gdb.graph_map(self.NEO4J_CONFIG)
        vertexes, edges = graph.create_vertices( image_resize )
        
        graph.create_new_database(map_id)
        graph.create_nodes(vertexes, edges, map_id, floor)
    
    def find_path(self, map_id, wp):
        map = self.db.get_slam_map(map_id)
        print(map[3])

        #read yaml file
        yaml_file_name = map[3]
        response = self.s3.Object(self.BUCKETNAME, yaml_file_name).get()
        body = response['Body'].read()
        yaml_obj = yaml.safe_load(body.decode('utf-8'))
        yamlData = {
                    "origin": {"x": yaml_obj['origin'][0], "y": yaml_obj['origin'][1]},
                    "resolution": {"x": yaml_obj['resolution'], "y": yaml_obj['resolution']},
                    "direction": {"x": 1, "y": -1}
        }

        #read pgm file
        pgm_file_name = map[2]
        response = self.s3.Object(self.BUCKETNAME, pgm_file_name).get()
        pgm_data = response['Body'].read()
        inst = io.BytesIO(pgm_data)
        # => <class 'bytes'>
        #image = Image.open(inst)
        #print(image.format)
        #print(image.mode)
        
        image = cv2.imdecode(
            np.array(bytearray(pgm_data), dtype=np.uint8),
            cv2.IMREAD_COLOR
            #cv2.IMREAD_UNCHANGED
        )

        #
        image_resize = cv2.resize(image, dsize=None, fx=0.5, fy=0.5)
        print("map size ", image_resize.shape)

        # path
        floor = 1
        graph = path.voronoi2gdb.graph_map(self.NEO4J_CONFIG)
        edges = graph.read_graph(map_id)
        print(len(edges))

        #start = (0, 1100, 1050) # z, y, x
        #start_pp = path.pathpoint.path_point(start, 1, "start", "")

        #goal = (0, 840, 1440)
        #goal_pp = path.pathpoint.path_point(goal, 1, "goal", "")

        number_list = wp
        #number_list.insert(0,start_pp)
        #number_list.insert(len(number_list),goal_pp)

        #i = 0
        #WP1 = (float(number_list[i][0]),float(number_list[i][1]),float(number_list[i][2]))
        #WP2 = (float(number_list[i+1][0]),float(number_list[i+1][1]),float(number_list[i+1][2]))

        find = path.findingPath.find_path(edges, number_list, image_resize)
        #path_list, cost = find.a_star_graph(WP1,WP2,find.heuristic)
        path_list, cost = find.a_star_graph(wp[0], wp[-1],find.heuristic)

        #print( path_list )
        #print( cost )

        return(path_list, cost)

def server():
    server = grpc.server(
        futures.ThreadPoolExecutor(max_workers=10),
        options=(
        #    ('grpc.keepalive_time_ms', 30000),
        #    ('grpc.keepalive_timeout_ms', 30000),
        #    ('grpc.keepalive_permit_without_calls', True),
        #    ('grpc.http2.max_pings_without_data', 0),
        #    ('grpc.http2.min_time_between_pings_ms',10000),
            ('grpc.http2.min_ping_interval_without_data_ms', 10000),
        )
    )
    grpc_proto.graph_pb2_grpc.add_GraphMapServiceServicer_to_server(graph_grpc(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    print('start server listen in...')
    server.wait_for_termination()

if __name__ == '__main__':
    server()

    #test
    #graphtest = graph_proc()
    #edges = graphtest.get_graph(36)
    #for e in edges:
    #    for p in e.p2p:
    #        print(p.pos)

    #graphtest.create_new_graph(36)
    #graphtest.find_path(36,[])

