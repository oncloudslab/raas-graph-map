import sys,pathlib
root_dir = pathlib.Path(__file__).resolve().parents[0]
sys.path.append(str(root_dir))

from . import DBModel