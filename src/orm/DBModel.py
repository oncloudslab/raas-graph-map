#!/usr/bin/python3
# -*- coding: utf-8 -*-
import MySQLdb

class MapDB:
    """docstring for MapDB
    カーソルで取るべきか悩んでる
    複数スレッドからのアクセスを考えると、カーソルを生成するメソッドが必要
    その場合、異常時にしっかりクローズできるようにしないといけない
    """
    def __init__(self, addr, port, user, password, database):
        self.addr     = addr
        self.port     = port
        self.user     = user
        self.password = password
        self.database = database
        self.con = self._init_db()

    def __del__(self):
        self.con.close()

    def get_slam_map(self, mapID):
        cur = self._get_cursor()
        cur.execute("""SELECT `map-id`, `version`, `pgm_path`, `YAML_path`,
          `regist-date`, `regist-user-id`
          FROM slam_map
          WHERE `map-id` = %s""", (mapID,))
        result = cur.fetchone()
        cur.close()
        return result

    def get_slam_map_ALL(self):
        cur = self._get_cursor()
        cur.execute("""SELECT * FROM slam_map""")

        result = cur.fetchall()
        cur.close()
        return result

    def _get_cursor(self):
        try:
            self.con.ping()
        except MySQLdb._exceptions.Error as err:
            # reconnect your cursor as you did in __init__ or wherever
            self.con = self._init_db()
        return self.con.cursor()

    def _init_db(self):
        con = MySQLdb.connect(
                user=self.user,
                passwd=self.password,
                host=self.addr,
                port=self.port,
                db=self.database,
                charset="utf8")
        # reconnect option is not supported
        return con
