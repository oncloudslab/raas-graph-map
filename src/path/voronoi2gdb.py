#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
import time
import yaml
import cv2
from statistics import mean
from neo4j import GraphDatabase
import numpy as np
from scipy.spatial import Voronoi
from bresenham import bresenham
import pathpoint

class graph_map:
    graphDB_Driver = None

    def __init__(self, _neo4jDir):
        with open(_neo4jDir, 'r', encoding='utf-8') as neo4j:
            neo4jstring = neo4j.read()
            neo4j_obj = yaml.safe_load(neo4jstring)
            neo4jData = {
                        "uri": neo4j_obj['uri'],
                        "userName": neo4j_obj['userName'],
                        "password": neo4j_obj['password']
                }

        #Neo4j Enterprise
        uri= neo4jData['uri']
        userName= neo4jData['userName']
        password= neo4jData['password']

        #Connect to the neo4j database server
        self.graphDB_Driver  = GraphDatabase.driver(uri, auth=(userName, password))

    def get_unique_list(self,seq):
        seen = []
        return [x for x in seq if x not in seen and not seen.append(x)]

    def create_vertices(self, map):
        '''
        Create a vertice database by reading a PNG image
        '''
        #pre processing
        kernel = np.ones((2,2), np.uint8)
        map = cv2.dilate(map, kernel, iterations=1)
        kernel = np.ones((2,2), np.uint8)
        map = cv2.erode(map, kernel, iterations=1)
        ret, map = cv2.threshold(map, 200, 255, cv2.THRESH_BINARY)

        #minimum and maximum north coordinates
        north_min = 0
        north_max = map.shape[0]

        #minimum and maximum east coordinates
        east_min = 0
        east_max = map.shape[1]

        #given the minimum and maximum coordinates we can
        #calculate the size of the grid
        north_size = int(north_max - north_min)
        east_size = int(east_max - east_min)

        #Initialize an empty grid
        grid = np.zeros((north_size, east_size))
        #Initialize an empty list for Voronoi points
        points = []

        #white in RGB
        #Todo map 
        #if( map.shape[2] == 3 ):
        pixel = (0,0,0)
        best = (0, 0, 0)
        
        start_time = time.perf_counter()
        print("start create list of obstacle", start_time)
        for x in range (map.shape[0] ):
            for y in range (map.shape[1] ):
                #print( x, y, map.getpixel ( (x, y) ) )
                ipixel = map[(y,x)]
                if (tuple(ipixel) == pixel):
                    #best = (0, x, y)
                    #x, y = best[1:]
                    #add center of obstacles to points list
                    points.append([x, y])

                #if (mean(tuple(ipixel)) < 220 )
                    #Populate the grid with obstacles
                    #grid[x, y] = 1

        #create a voronoi graph based on location of obstacle centres
        execution_time = time.perf_counter() - start_time
        print("start voronoi..", execution_time)
        print("obstale ", len(points))

        graph = Voronoi(points)

        #check each edge from graph.ridge_vertices for distance field map
        execution_time = time.perf_counter() - start_time
        print("check edges for collision..", execution_time)

        edges = []
        for v in graph.ridge_vertices:
            p1 = graph.vertices[v[0]]
            p2 = graph.vertices[v[1]]
            cells = list(bresenham(int(p1[0]), int(p1[1]), int(p2[0]), int(p2[1])))
            hit = False

            cost = 0
            for c in cells:
                #First check if we're off the map
                if np.amin(c) < 0 or c[0] >= grid.shape[0] or c[1] >= grid.shape[1]:
                    hit = True
                    break
                #Next check if we're in collision
                #if grid[c[0], c[1]] == 1:
                #if mean(tuple(map[c[0], c[1]])) < 50:
                #    hit = True
                #    break

            #If the edge does not hit on obstacle
            #add it to the list
            if not hit:
                #array to tuple for future graph creation step
                p1 = (p1[0], p1[1])
                p2 = (p2[0], p2[1])
                edges.append((p1, p2))

        execution_time = time.perf_counter() - start_time
        print("create vertexes and edges done.", execution_time)


        v = []
        for e in edges:
            v.append(e[0])
            v.append(e[1])

        vertexes = self.get_unique_list(v)


        execution_time = time.perf_counter() - start_time
        print("done.", execution_time)



        return vertexes, edges

    def show_databases(self):
        with self.graphDB_Driver.session(database="system") as tx:
            res = tx.run('SHOW DATABASES')
            for r in res:
                print(r)
                print(r.keys())
                print(r['name'])

    def create_new_database(self,id_map):
        print("start create new database..")
        db_name = 'map' + str(id_map)
        #check already database existed and create new database
        with self.graphDB_Driver.session(database="system") as tx:
            s = "CREATE DATABASE " + db_name + " IF NOT EXISTS"
            res = tx.run(s)
            #res = tx.run("show databases")
            #dbExisted = False
            #for r in res:
            #    if( r['name'] == db_name ):
            #        dbExisted == True
            #        break

            #if( dbExisted == False ):
            #    print("create new db:", db_name)
            #else:
            #    print(db_name, " is already existed")

    def delete_database(self,id_map):
        db_name = 'map' + str(id_map)
        #check already database existed and create new database
        with self.graphDB_Driver.session(database="system") as tx:
            s = "DROP DATABASE " + db_name + " IF EXISTS"
            res = tx.run(s)

    def create_nodes(self,points,edges,id_map,_floor_map):
        '''
        Create NODES in NOE4J
        '''
        print("start create node..")

        db_name = 'map'+str(id_map)
        vertex_name = 'path'
        edge_name = 'heuristics'
        #Delete all the nodes and relationships
        clean = """
        MATCH (n)
        DETACH DELETE n
        """
        with self.graphDB_Driver.session(database=db_name) as graphDB_Session:
            clean_nodes = graphDB_Session.run(clean)
            print("All nodes and relationships are cleaned...")
            print("New nodes and relationships are being added...")

            floor = str(_floor_map)
            action = ""
            attribute = ""

            print("creating vertexs ", len(points))
            z = str(0)
            i = 0
            for p in enumerate(points):
                x = str(p[1][0])
                y = str(p[1][1])

                #Cypher statement, add variables in the statement
                Obs = 'CREATE (p:'+vertex_name +'{z:"'+ z +'",y:"'+ y +'",x:"'+ x +'",Floor:"'+ floor +'",Action:"'+ action +'",Attribute:""})'
                #Start cypher statement
                create_n_obs = graphDB_Session.run(Obs)
                if( i%1000 == 0 ):
                    print(i, "/", len(edges))
                i=i+1

            print("creating edges ", len(edges))
            z1 = z2 = str(0)
            i = 0
            for e in enumerate(edges):
                x1 = str(e[1][0][0])
                y1 = str(e[1][0][1])
                x2 = str(e[1][1][0])
                y2 = str(e[1][1][1])
                h = ( (float(z1)-float(z2))**2 +(float(y1)-float(y2))**2 +(float(x1)-float(x2))**2 )**0.5
                heuristics = str(h)

                #Cypher statement, add variables in the statement
                vertex1 = '(s:'+vertex_name +'{z:"'+ z1 +'",y:"'+ y1 +'",x:"'+ x1 +'",Floor:"'+ floor +'",Action:"",Attribute:""})'
                vertex2 = '(e:'+vertex_name +'{z:"'+ z2 +'",y:"'+ y2 +'",x:"'+ x2 +'",Floor:"'+ floor +'",Action:"",Attribute:""})'
                edge = 'CREATE ' + '(s)-[rel:'+edge_name +'{h:"'+ heuristics +'"}]->(e)'
                #Ver = 'CREATE (p1:' vertex_name +'{z:"'+ z1 +'",y:"'+ y1 +'",x:"'+ x1 +'",Floor:"'+ floor +'",Action:"'+ action +'",Identifier:"'+ identifier +'"})-[rel:heuristics_'+ _id_map +'{h:"'+ heuristics +'"}]->(p2:GDB_'+ _id_map +'{z:"'+ z2 +'",y:"'+ y2 +'",x:"'+ x2 +'",Floor:"'+ floor +'",Action:"'+ action +'",Identifier:"'+ identifier +'"})'
                Ver = 'MATCH ' + vertex1 + "," + vertex2 + " " + edge
                #Start cypher statement
                create_n_r = graphDB_Session.run(Ver)
                if( i%100 == 0 ):
                    print(i, "/", len(edges))
                i=i+1
            
            print("finish creating nodes")

    def READ_NODES_2EDGES(self, id_map):
        points = []
        edges  = []
        elevs  = []
            
        db_name = 'map'+str(id_map)
        vertex_name = 'path'
        edge_name = 'heuristics'

        with self.graphDB_Driver.session(database=db_name) as graphDB_Session:
            Ver_cal='MATCH (n)-[r]->(m) WHERE labels(n)=["'+ vertex_name +'"] RETURN n,r,m'
            nodes = graphDB_Session.run(Ver_cal)
        
            for node in nodes:
                #print(str(node[0]['z']), node[0]['y'], node[0]['x'],node[0]['Action'], node[0]['Identifier'], node[0]['Floor'])
                p1 = (float(node[0]['z']), float(node[0]['y']), float(node[0]['x']))
                p2 = (float(node[2]['z']), float(node[2]['y']), float(node[2]['x']))
                edges.append((p1, p2))
                
        return edges

    def read_graph(self, id_map):
        points = []
        edges  = []
        elevs  = []
            
        db_name = 'map'+str(id_map)
        vertex_name = 'path'
        edge_name = 'heuristics'

        with self.graphDB_Driver.session(database=db_name) as graphDB_Session:
            Ver_cal='MATCH (n)-[r]->(m) WHERE labels(n)=["'+ vertex_name +'"] RETURN n,r,m'
            nodes = graphDB_Session.run(Ver_cal)
        
            for node in nodes:
                #print(str(node[0]['z']), node[0]['y'], node[0]['x'],node[0]['Action'], node[0]['Identifier'], node[0]['Floor'])
                #p1 = (float(node[0]['z']), float(node[0]['y']), float(node[0]['x']))
                #p2 = (float(node[2]['z']), float(node[2]['y']), float(node[2]['x']))
                pos1 = (float(node[0]['z']), float(node[0]['y']), float(node[0]['x']))
                p1 = pathpoint.path_point( pos1, node[0]['Floor'], node[0]['Attribute'] )
                pos2 = (float(node[2]['z']), float(node[2]['y']), float(node[2]['x']))
                p2 = pathpoint.path_point( pos2, node[2]['Floor'], node[2]['Attribute'] )
                #p2 = (float(node[2]['z']), float(node[2]['y']), float(node[2]['x']))
                edge = pathpoint.edge((p1, p2), node[1]['h'])
                edges.append(edge)
                
        return edges

if __name__ == '__main__':
    print("voronoi2gdb.py main proc start")