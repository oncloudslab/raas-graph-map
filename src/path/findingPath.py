#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
import numpy as np
import networkx as nx
import sys,os,time,itertools
from queue import PriorityQueue
from scipy import spatial
from bresenham import bresenham
from statistics import mean
import pathpoint

sys.setrecursionlimit(5000) 

class find_path():
    graph = None
    map = None

    def __init__(self, edges, waypoints, map):
        print("init")
        #WP_closest
        for w in waypoints:
            dist_w = 1000000
            W = w.pos
            for i in range(len(edges)):
                #edges[i].print()
                for j in (edges[i].p2p):
                    p = j.pos
                    if (p[0]==W[0]):
                        d = self.heuristic(W, p)
                        if d < dist_w:
                            WP = j
                            dist_w = d

            edge = pathpoint.edge((WP, w), dist_w)
            edges.append(edge)
            #edges.append((WP,w))

        print("map size ", map.shape)
        self.map = map
        self.graph = self.create_graph(edges)

    def heuristic_cost(self, n1, n2):
        n1_x = int(n1[2])
        n1_y = int(n1[1])
        n2_x = int(n2[2])
        n2_y = int(n2[1])
        cells = list(bresenham(n1_x, n1_y, n2_x, n2_y))
        sum = 0
        for c in cells:
            x = int(c[0])
            y = int(c[1])
            c = (255 - mean(self.map[y,x]))
            c = max(c,1)
            c = min(c,50)
            sum = sum + c

        return(sum)

    def heuristic(self, n1, n2):
        return ( (float(n1[0])-float(n2[0]))**2 +(float(n1[1])-float(n2[1]))**2 +(float(n1[2])-float(n2[2]))**2 )**0.5

    def create_graph(self,edges):
        graph = nx.Graph()
        for elem in edges:
            p1 = elem.p2p[0]
            p2 = elem.p2p[1]
            dist = self.heuristic_cost(p1.pos, p2.pos)
            #dist = elem.cost
            graph.add_edge(p1, p2, weight=dist)
        return graph

    def a_star_graph(self, _start_pos, _goal_pos, h):
        path = []
        path_cost = 0
        queue = PriorityQueue()
        queue.put((0, _start_pos))
        visited = set([_start_pos])

        branch = {}
        found = False
        while not queue.empty():
            item = queue.get()
            current_node = item[1]
            if current_node == _start_pos:
                current_cost = 0.0
            else:
                current_cost = branch[current_node][0]

            if current_node == _goal_pos:
                print('Found a path.')
                found = True
                break
            else:
                for next_node in self.graph[current_node]:
                    cost = self.graph.edges[current_node, next_node]['weight']
                    branch_cost = current_cost + cost
                    queue_cost = branch_cost + h(next_node.pos, _goal_pos.pos)

                    if next_node not in visited:
                        visited.add(next_node)
                        branch[next_node] = (branch_cost, current_node)
                        queue.put((queue_cost, next_node))
                        
        if found:
            # retrace steps
            n = _goal_pos
            path_cost = branch[n][0]
            path.append(_goal_pos)
            while branch[n][1] != _start_pos:
                path.append(branch[n][1])
                n = branch[n][1]
            path.append(branch[n][1])
        else:
            print('**********************')
            print('Failed to find a path!')
            print('**********************')
        return path[::-1], path_cost

if __name__ == "__main__":
    print("main")