#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
class path_point:
    def __init__(self, p, floor, att):
        self.pos = p
        self.floor = floor
        self.attribute = att

    def __eq__(self, other):
        if not isinstance(other, path_point):
            return NotImplemented
        return self.pos == other.pos

    def __hash__(self):
        return hash(self.pos)

    def print(self):
        print("path_point")
        print("pos", self.pos ,"(z,y,x)")
        print("floor", self.floor)
        print("attribute", self.attribute)
    
class edge:
    def __init__(self, p2p, h ):
        self.p2p = p2p
        self.cost = h

    def print(self):
        print("edge")
        for p in self.p2p:
            print("p2p")
            p.print()
        
        print("cost", self.cost)        