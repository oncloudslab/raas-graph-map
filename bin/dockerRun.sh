#!/bin/bash

usage_exit() {
	echo "Usage: $0 [-n] -c container name -d rosbag share dir script_file_name.sh" 1>&2
	echo "-i: terminal mode ( not start api server)"
	echo "-n: nvidia"
	exit 1
}

COMMAND='cd /root/robot-api/src && /root/.nvm/versions/node/v14.15.5/bin/node /root/robot-api/src/index.js'
OPT_API='-d'

while getopts inc:h OPT
do
	case $OPT in
        n) TARGET=nvidia
            echo "set docker command: nvidia"
            OPT_GPU='--gpus all'
			DOCKER=docker
            ;;
		i) COMMAND='/bin/bash'
		    OPT_API='-it'
		    echo "terminal mode"
		    ;;
        c) CONTAINER_NAME=$OPTARG
            ;;
		h) usage_exit
		    ;;
		\?) usage_exit
		    ;;
	esac
done

if [ ! $TARGET ]; then
	echo "set docker command: docker ( general )"
	DOCKER=docker
fi
	
if [ ! $CONTAINER_NAME ]; then
	usage_exit
fi

echo "Set Container name : $CONTAINER_NAME"

VAR_RUN=$(docker ps --format {{.Names}} | grep -x $CONTAINER_NAME)
VAR_SLEEP=$(docker ps -a --format {{.Names}} | grep -x $CONTAINER_NAME)

if [ "$VAR_RUN" = "$CONTAINER_NAME" ] || [ "$VAR_SLEEP" = "$CONTAINER_NAME" ]; then
	echo "Error: Docker componet is already existed, Please docker rm $CONTAINER_NAME"
	exit
fi

USER=root
HOST_SRC_DIR=`readlink -f ../`
SRC_DIR=/$USER/graph-map

IMAGE_NAME=graph-map
$DOCKER run \
	--rm --privileged --net=host \
	${OPT_API} \
	${OPT_GPU} \
	--user ${USER} \
	--name=${CONTAINER_NAME} \
	--env=TZ=Asia/Tokyo \
	--volume=${HOST_SRC_DIR}:${SRC_DIR}:rw \
	--env="DISPLAY" --env="QT_X11_NO_MITSHM=1" -v="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    $IMAGE_NAME /bin/bash -c "${COMMAND}"
