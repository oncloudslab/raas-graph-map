#!/usr/bin/python3
# -*- coding: utf-8 -*-
from os import sync
import sys
import boto3
import json
import yaml
from PIL import Image
import io
import cv2
import numpy as np
import pathlib
from matplotlib import pyplot as plt

root_dir = pathlib.Path(__file__).resolve().parents[1]
sys.path.append(str(root_dir)+"/src/")

#gRPC
import grpc
import grpc_proto
from concurrent import futures

#raas lib
import path
import orm

class graph:
    db = None
    s3 = None
    BUCKETNAME = 'onclouds-raas-dev'
    GRAPTH_SERVER = '52.194.226.4:50051'

    #neo4j
    NEO4J_CONFIG = '../src/Neo4j_enterprise.yaml'

    def __init__(self):
        #aws
        boto3config = json.load(open('../src/passwd-s3fs.json'))
        session = boto3.Session(aws_access_key_id=boto3config['accessKeyId'],
                  aws_secret_access_key=boto3config['secretAccessKey'],
                  region_name='ap-northeast-1')

        #s3
        self.s3 = session.resource('s3')
        for bucket in self.s3.buckets.all():
            print(bucket.name)

        #DB ( ORM )
        with open('../src/db-config.json', 'r', encoding='utf-8') as conf:
            config = json.load(conf)
            DBconf = config[config['env']]['mapDB']
            self.db = orm.DBModel.MapDB(DBconf['host'], DBconf['port'], DBconf['username'],
                       DBconf['password'], DBconf['database'])

    def get_map_image(self, map_id):
        map = self.db.get_slam_map(map_id)
        #read yaml file
        yaml_file_name = map[3]
        response = self.s3.Object(self.BUCKETNAME, yaml_file_name).get()
        body = response['Body'].read()
        yaml_obj = yaml.safe_load(body.decode('utf-8'))
        yamlData = {
                    "origin": {"x": yaml_obj['origin'][0], "y": yaml_obj['origin'][1]},
                    "resolution": {"x": yaml_obj['resolution'], "y": yaml_obj['resolution']},
                    "direction": {"x": 1, "y": -1}
        }

        #read pgm file
        pgm_file_name = map[2]
        response = self.s3.Object(self.BUCKETNAME, pgm_file_name).get()
        pgm_data = response['Body'].read()
        inst = io.BytesIO(pgm_data)
        # => <class 'bytes'>
        #image = Image.open(inst)
        #print(image.format)
        #print(image.mode)
        
        image = cv2.imdecode(
            np.array(bytearray(pgm_data), dtype=np.uint8),
            cv2.IMREAD_COLOR
            #cv2.IMREAD_UNCHANGED
        )

        #
        image_resize = cv2.resize(image, dsize=None, fx=0.5, fy=0.5)
        print("map size ", image_resize.shape)
        return image_resize

    def create_graph(self, facility, floor):
        print("create_graph")
        #Graph server
        with grpc.insecure_channel(self.GRAPTH_SERVER) as channel:
            stub = grpc_proto.graph_pb2_grpc.GraphMapServiceStub(channel)

            request = grpc_proto.graph_pb2.MapRequest(facility_id=str(facility), floor=str(floor))
            print(request)
            try:
                response = stub.createGraph(request)
            except grpc.RpcError as e:
                e.details()
                status_code = e.code()
                status_code.name
                status_code.value
                print(status_code)

            print(response)

    def get_graph(self, facility, floor):
        #Graph server
        with grpc.insecure_channel(self.GRAPTH_SERVER) as channel:
            stub = grpc_proto.graph_pb2_grpc.GraphMapServiceStub(channel)

            request = grpc_proto.graph_pb2.MapRequest(facility_id=str(facility), floor=str(floor))
            response = stub.getGraph(request)
            #print(response)
        
        #conver to pathpoint edge
        edges = []
        for e in response.edges:
            pos1 = (float(e.p2p[0].pose.z), float(e.p2p[0].pose.y), float(e.p2p[0].pose.x))
            p1 = path.pathpoint.path_point( pos1, 1, '', '' )
            pos2 = (float(e.p2p[1].pose.z), float(e.p2p[1].pose.y), float(e.p2p[1].pose.x))
            p2 = path.pathpoint.path_point( pos2, 1, '', '' )
            edges.append( path.pathpoint.edge((p1,p2), e.cost))

        return(response.vertexes, edges)

    # copy from pathpoint class to grpc
    def copy_pathpoint(self,p):
        pos = grpc_proto.raas_pb2.Point3d(x=str(p.pos[2]), y=str(p.pos[1]), z=str(p.pos[0]))
        pp = grpc_proto.raas_pb2.PointPath(pose=pos, floor=str(p.floor), attribute=str(p.attribute), action=str(p.action))
        return pp

    def find_path(self, facility, floor):
        #start 
        start = (0, 1100, 1050) # z, y, x
        start_pp = path.pathpoint.path_point(start, 1, "start", "")

        goal = (0, 840, 1440)
        goal_pp = path.pathpoint.path_point(goal, 1, "goal", "")

        waypoints = []
        waypoints.append(self.copy_pathpoint(start_pp))
        waypoints.append(self.copy_pathpoint(goal_pp))

        pp = []
        with grpc.insecure_channel(self.GRAPTH_SERVER) as channel:
            stub = grpc_proto.graph_pb2_grpc.GraphMapServiceStub(channel)

            request = grpc_proto.graph_pb2.PathRequest(waypoints=waypoints, facility=str(facility), floor=str(floor))
            print(request)
            response = stub.findPath(request)

            for grpc_point in response.path:
                pos = (float(grpc_point.pose.z), float(grpc_point.pose.y), float(grpc_point.pose.x))
                pp.append( path.pathpoint.path_point( pos, grpc_point.floor, grpc_point.attribute, grpc_point.action ) )
 
        return(pp)

def plot_edges(edges, map):
    for edge in edges:
        #print(edge.p2p[0])
        p1 = edge.p2p[0].pos
        p2 = edge.p2p[1].pos
        plt.plot([p1[2], p2[2]], [p1[1], p2[1]], 'r-')
    
    #plt.plot([0, 50], [0, 100], 'r-')
    plt.imshow(map)
    plt.show()

def plot_path(path, map):
    for i in range(len(path)-1):
        p1 = path[i].pos
        p2 = path[i+1].pos
        plt.plot([p1[2], p2[2]], [p1[1], p2[1]], 'r-')
    
    plt.imshow(map)
    plt.show()

def debug_main():
    graph_debug = graph()
    
    #create new graph
    graph_debug.create_graph(1,36)

    #map_image = graph_debug.get_map_image(36)
    #vertexes, edges = graph_debug.get_graph(1,36)
    #plot_edges(edges, map_image)

    #path = graph_debug.find_path(1,36)
    #plot_path(path, map_image)
    
if __name__ == '__main__':
    debug_main()
    #client()
