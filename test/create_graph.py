#!/usr/bin/python3
# -*- coding: utf-8 -*-
from os import sync
from statistics import mean
import sys, subprocess, time
import boto3
import json
import yaml
from PIL import Image
import io
import cv2
import numpy as np
import pathlib
from matplotlib import pyplot as plt
from scipy import ndimage as ndi
#import skan
#from skan import draw
#from skimage import morphology
#from skan.pre import threshold
from bresenham import bresenham
import networkx as nx
from queue import PriorityQueue

root_dir = pathlib.Path(__file__).resolve().parents[1]
sys.path.append(str(root_dir)+"/src/")

#gRPC
import grpc
import grpc_proto
from concurrent import futures

#raas lib
import path
import orm

class graph:
    db = None
    s3 = None
    BUCKETNAME = 'onclouds-raas-dev'
    GRAPTH_SERVER = '52.194.226.4:50051'

    #neo4j
    NEO4J_CONFIG = '../src/key/Neo4j_enterprise.yaml'

    def __init__(self):
        #aws
        boto3config = json.load(open('../src/key/passwd-s3fs.json'))
        session = boto3.Session(aws_access_key_id=boto3config['accessKeyId'],
                  aws_secret_access_key=boto3config['secretAccessKey'],
                  region_name='ap-northeast-1')

        #s3
        self.s3 = session.resource('s3')
        for bucket in self.s3.buckets.all():
            print(bucket.name)

        #DB ( ORM )
        with open('../src/db-config.json', 'r', encoding='utf-8') as conf:
            config = json.load(conf)
            DBconf = config[config['env']]['mapDB']
            self.db = orm.DBModel.MapDB(DBconf['host'], DBconf['port'], DBconf['username'],
                       DBconf['password'], DBconf['database'])

    def get_map_image(self, map_id):
        print("get_map_image")
        map = self.db.get_slam_map(map_id)
        print("orm db done")
        print(map)
        #read yaml file
        #yaml_file_name = map['YAML_path'] 
        yaml_file_name = map[3]
        response = self.s3.Object(self.BUCKETNAME, yaml_file_name).get()
        body = response['Body'].read()
        yaml_obj = yaml.safe_load(body.decode('utf-8'))
        yamlData = {
                    "origin": {"x": yaml_obj['origin'][0], "y": yaml_obj['origin'][1]},
                    "resolution": {"x": yaml_obj['resolution'], "y": yaml_obj['resolution']},
                    "direction": {"x": 1, "y": -1}
        }

        print("connect to aws")
        #read pgm file
        #pgm_file_name = map['pgm_path']
        pgm_file_name = map[2]
        response = self.s3.Object(self.BUCKETNAME, pgm_file_name).get()
        pgm_data = response['Body'].read()
        inst = io.BytesIO(pgm_data)
        # => <class 'bytes'>
        #image = Image.open(inst)
        #print(image.format)
        #print(image.mode)
        print("connect to aws done")
        
        image = cv2.imdecode(
            np.array(bytearray(pgm_data), dtype=np.uint8),
            cv2.IMREAD_COLOR
            #cv2.IMREAD_UNCHANGED
        )

        #
        image_resize = cv2.resize(image, dsize=None, fx=0.5, fy=0.5)
        print("map size ", image_resize.shape)
        return image_resize

    def create_graph(self, facility, floor):
        #Graph server
        opts = (
            ('grpc.keepalive_time_ms', 60000),
            ('grpc.keepalive_timeout_ms', 30000),
            ('grpc.keepalive_permit_without_calls', True),
            ('grpc.http2.max_pings_without_data', 0),
            ('grpc.http2.min_time_between_pings_ms',30000),
            ('grpc.http2.min_ping_interval_without_data_ms', 5000),
        )

        with grpc.insecure_channel(self.GRAPTH_SERVER, options=opts) as channel:
            stub = grpc_proto.graph_pb2_grpc.GraphMapServiceStub(channel)

            request = grpc_proto.graph_pb2.MapRequest(facility_id=str(1), floor=str(36))
            print(request)
            response = stub.createGraph(request)
            print(response)

        print("close")

    def get_graph(self, facility, floor):
        print("get_graph")
        #Graph server
        with grpc.insecure_channel(self.GRAPTH_SERVER) as channel:
            stub = grpc_proto.graph_pb2_grpc.GraphMapServiceStub(channel)

            request = grpc_proto.graph_pb2.MapRequest(facility_id=str(facility), floor=str(floor))
            response = stub.getGraph(request)
            #print(response)
        
        print("done")
        #conver to pathpoint edge
        edges = []
        for e in response.edges:
            pos1 = (float(e.p2p[0].floor_p.pos.z), float(e.p2p[0].floor_p.pos.y), float(e.p2p[0].floor_p.pos.x))
            p1 = path.pathpoint.path_point( pos1, 1, '')
            pos2 = (float(e.p2p[1].floor_p.pos.z), float(e.p2p[1].floor_p.pos.y), float(e.p2p[1].floor_p.pos.x))
            p2 = path.pathpoint.path_point( pos2, 1, '')
            edges.append( path.pathpoint.edge((p1,p2), e.cost))

        return(response.vertexes, edges)

    # copy from pathpoint class to grpc
    def copy_pathpoint(self,p):
        pos = grpc_proto.raas_pb2.Point3d(x=str(p.pos[2]), y=str(p.pos[1]), z=str(p.pos[0]))
        pp = grpc_proto.raas_pb2.PointPath(pose=pos, floor=str(p.floor), attribute=str(p.attribute), action=str(p.action))
        return pp

    def find_path(self, facility, floor):
        #start 
        start = (0, 1100, 1050) # z, y, x
        start_pp = path.pathpoint.path_point(start, 1, "start", "")

        goal = (0, 840, 1440)
        goal_pp = path.pathpoint.path_point(goal, 1, "goal", "")

        waypoints = []
        waypoints.append(self.copy_pathpoint(start_pp))
        waypoints.append(self.copy_pathpoint(goal_pp))

        pp = []
        with grpc.insecure_channel(self.GRAPTH_SERVER) as channel:
            stub = grpc_proto.graph_pb2_grpc.GraphMapServiceStub(channel)

            request = grpc_proto.graph_pb2.PathRequest(waypoints=waypoints, facility=str(facility), floor=str(floor))
            print(request)
            response = stub.findPath(request)

            for grpc_point in response.path:
                pos = (float(grpc_point.pose.z), float(grpc_point.pose.y), float(grpc_point.pose.x))
                pp.append( path.pathpoint.path_point( pos, grpc_point.floor, grpc_point.attribute, grpc_point.action ) )
 
        return(pp)

def sharp_kernel(k):
    return np.array([
        [-k/9, -k/9, -k/9],
        [-k/9, 1+8*k/9, k/9],
        [-k/9, -k/9, -k/9]
    ], np.float32)

def heuristic(n1, n2, map):
    #return ( (float(n1[0])-float(n2[0]))**2 +(float(n1[1])-float(n2[1]))**2 )**0.5
    cells = list(bresenham(int(n1[0]), int(n1[1]), int(n2[0]), int(n2[1])))
    sum = 0
    for c in cells:
        x = int(c[0])
        y = int(c[1])
        c = (255 - mean(map[y,x]))
        sum = sum + c

    return(sum)

def plot_map(map):

    #resize
    img_gray = cv2.cvtColor(map, cv2.COLOR_BGR2GRAY).astype("uint8")

    #voroni
    voronoi = path.voronoi2gdb.graph_map("../src/key/Neo4j_enterprise.yaml")
    vgraph, vertexes, edges = voronoi.create_vertices( map )

    #check each edge from graph.ridge_vertices for collision
    print("check edges for collision..")
    edges = []
    graph = nx.Graph()

    start = (1050, 1100) 
    goal = (1440, 840)

    for v in vgraph.ridge_vertices:
        p1 = vgraph.vertices[v[0]]
        p2 = vgraph.vertices[v[1]]
        cells = list(bresenham(int(p1[0]), int(p1[1]), int(p2[0]), int(p2[1])))

        hit = False
        for c in cells:
            #First check if we're off the map
            if np.amin(c) < 0 or c[0] >= map.shape[0] or c[1] >= map.shape[1]:
                hit = True
                break            

        if not hit:
            sum = 0
            for c in cells:
                x = int(c[0])
                y = int(c[1])
                c = (255 - mean(map[y,x]))
                sum = sum + c

            #graph.add_edge(tuple(p1), tuple(p2), weight=heuristic(p1,p2))
            graph.add_edge(tuple(p1), tuple(p2), weight=float(sum))
            #plt.plot([p1[0], p2[0]], [p1[1], p2[1]], 'r-')

            #if( sum/len(cells) < 20 ):
                #edges.append((p1, p2))
                #print("cells")
                #print(len(cells))
                #print(map[(int(p1[1]),int(p1[0]))])
                #print(c, map((int(c[1]),int(c[0]))))

                #print(sum)
                #print(len(cells))
        
    print("aster")
    print(s)
    print(e)

    path_aster, cost_aster = a_star_graph(graph, s, e, map)

    print(len(path_aster))
    for i in range(len(path_aster)-1):
        print([path_aster[i][0], path_aster[i][0]], [path_aster[i+1][1], path_aster[i+1][1]])
        plt.plot([path_aster[i][0], path_aster[i+1][0]], [path_aster[i][1], path_aster[i+1][1]], 'r-')

    print(cost_aster)

    plt.imshow(map)
    plt.show()

        #for c in cells:
        #    print(c[0],c[1])
        #grid[c[0], c[1]] == 1

    #SKAN
    #ret, img_bin = cv2.threshold(img_gray, 220, 1, cv2.THRESH_BINARY)
    #skeleton0, distance = morphology.medial_axis(img_bin,return_distance=True)
    #skeleton0 = morphology.skeletonize(img_bin)
    #branch_stat = skan.summarize(skan.Skeleton(skeleton0, unique_junctions=False))
    #fig, axes = plt.subplots(1,2)
    #draw.overlay_skeleton_2d(img_gray, skeleton0, dilate=1, axes = axes[0])
    #draw.overlay_euclidean_skeleton_2d(img_gray, branch_stat, skeleton_color_source="branch-type")

    #print(branch_stat)

    #ret, img_bin = cv2.threshold(img_gray, 220, 255, cv2.THRESH_BINARY)
    #img_distance, idx = ndi.distance_transform_edt(img_bin, return_distances=True, return_indices=True)
    #morph_laplace_img = ndi.morphological_laplace(img_distance, (5,5))
    #skeleton = morph_laplace_img < morph_laplace_img.min()/8
    #pixel_graph, coordinates, degree = skan.skeleton_to_csgraph(skeleton)
    #stats = skan.summarize(skan.Skeleton(skeleton))

    #kernel = sharp_kernel(9)
    #print(kernel)
    #img_sharp = cv2.filter2D(img_gray, -1, kernel).astype("uint8")

    #plt.imshow(img_distance, cmap="gray")
    #plt.imshow(skeleton0, cmap="gray")
    #plt.imshow(cv2.add(img_gray, img_edge), cmap="gray")
    #plt.show()

def plot_edges(edges, map):
    #distance feild map
    img_gray = cv2.cvtColor(map, cv2.COLOR_BGR2GRAY).astype("uint8")
    ret, img_bin = cv2.threshold(img_gray, 220, 255, cv2.THRESH_BINARY)
    distance_map, idx = ndi.distance_transform_edt(img_bin, return_distances=True, return_indices=True)

    #elimination
    for e in edges:
        p1_x = int(e.p2p[0].pos[2])
        p1_y = int(e.p2p[0].pos[1])
        p2_x = int(e.p2p[1].pos[2])
        p2_y = int(e.p2p[1].pos[1])
        cells = list(bresenham(int(p1_x), int(p1_y), int(p2_x), int(p2_y)))
        hit = False

        cost = 0
        for c in cells:
            #First check if we're off the map
            if np.amin(c) < 0 or c[0] >= map.shape[0] or c[1] >= map.shape[1]:
                hit = True
                break
            
            #Next check if we're in collision
            cost = cost + (255 - distance_map[c[1], c[0]])

        if( distance_map[p1_y,p1_x] < 10 and distance_map[p2_y,p2_x] < 10 ):
            hit = True 

        if not hit and cost < 50000:
            plt.plot([p1_x, p2_x], [p1_y, p2_y], 'r-')
    
    #plt.plot([0, 50], [0, 100], 'r-')
    plt.imshow(distance_map, cmap="gray")
    plt.show()

def image_filter(map):
    kernel = np.ones((2,2), np.uint8)
    map = cv2.dilate(map, kernel, iterations=1)
    kernel = np.ones((2,2), np.uint8)
    map = cv2.erode(map, kernel, iterations=1)
    ret, map = cv2.threshold(map, 200, 255, cv2.THRESH_BINARY)

    plt.imshow(map)
    plt.show()

def debug_main():
    graph_debug = graph()
    
    #create new graph
    #graph_debug.create_graph(1,36)

    map_image = graph_debug.get_map_image(36)
    #plot_map(map_image)
    vertexes, edges = graph_debug.get_graph(36,36)
    plot_edges(edges, map_image)

if __name__ == '__main__':
    debug_main()
