#!/usr/bin/python3
# -*- coding: utf-8 -*-
import boto3
import json
import yaml
import io
import cv2
import numpy as np
from matplotlib import pyplot as plt
import sys, pathlib

root_dir = pathlib.Path(__file__).resolve().parents[1]
sys.path.append(str(root_dir)+"/src/")

import orm
import path

class graph_proc:
    db = None
    s3 = None
    BUCKETNAME = 'onclouds-raas-dev'

    #neo4j
    NEO4J_CONFIG = '../src/Neo4j_enterprise.yaml'

    def __init__(self):
        #aws
        boto3config = json.load(open('../src/passwd-s3fs.json'))
        session = boto3.Session(aws_access_key_id=boto3config['accessKeyId'],
                  aws_secret_access_key=boto3config['secretAccessKey'],
                  region_name='ap-northeast-1')

        #s3
        self.s3 = session.resource('s3')
        for bucket in self.s3.buckets.all():
            print(bucket.name)

        #DB ( ORM )
        with open('../src/db-config.json', 'r', encoding='utf-8') as conf:
            config = json.load(conf)
            DBconf = config[config['env']]['mapDB']
            self.db = orm.DBModel.MapDB(DBconf['host'], DBconf['port'], DBconf['username'],
                       DBconf['password'], DBconf['database'])
    
    def plot(self, map_id):
        map = self.db.get_slam_map(map_id)

        #read yaml file
        yaml_file_name = map[3]
        response = self.s3.Object(self.BUCKETNAME, yaml_file_name).get()
        body = response['Body'].read()
        yaml_obj = yaml.safe_load(body.decode('utf-8'))
        yamlData = {
                    "origin": {"x": yaml_obj['origin'][0], "y": yaml_obj['origin'][1]},
                    "resolution": {"x": yaml_obj['resolution'], "y": yaml_obj['resolution']},
                    "direction": {"x": 1, "y": -1}
        }

        #read pgm file
        pgm_file_name = map[2]
        response = self.s3.Object(self.BUCKETNAME, pgm_file_name).get()
        pgm_data = response['Body'].read()
        inst = io.BytesIO(pgm_data)
        # => <class 'bytes'>
        #image = Image.open(inst)
        #print(image.format)
        #print(image.mode)
        
        image = cv2.imdecode(
            np.array(bytearray(pgm_data), dtype=np.uint8),
            cv2.IMREAD_COLOR
            #cv2.IMREAD_UNCHANGED
        )

        #
        image_resize = cv2.resize(image, dsize=None, fx=0.5, fy=0.5)
        print("map size ", image_resize.shape)

        neo4j = path.voronoi2gdb.graph_map(self.NEO4J_CONFIG)
        edges = neo4j.READ_NODES_2EDGES(map_id)

        for e in edges[0:3]:
            print(e)
            p1 = e[0]
            p2 = e[1]
            #print(p1)
            print([p1[2], p2[2]])
            plt.plot([p1[2], p2[2]], [p1[1], p2[1]], 'r-')

        #plt.plot([0, 50], [0, 100], 'r-')

        #plt.imshow(image_resize)
        plt.show()

if __name__ == '__main__':
    graph = graph_proc()
    graph.plot(36)
